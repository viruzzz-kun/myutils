from setuptools import find_packages, setup

setup(
    name='myutils',
    version='0.3.1',
    packages=find_packages(),
    url='',
    license='',
    author='viruzzz-kun',
    author_email='',
    description='',
    install_requires=[
        'python-dateutil',
        'six',
        'attrs',
        'blinker',
    ],
)
