import codecs
import logging
import struct
import traceback
from collections import (
    defaultdict,
    deque,
)
from contextlib import (
    contextmanager,
)


logger = logging.getLogger(__name__)

try:
    import gevent

    def _default_get_thread_id():
        return hash(gevent.getcurrent())

except ImportError:
    import threading

    def _default_get_thread_id():
        return threading.get_ident()


class Context(object):
    """
    The Context
    :type _manager: ContextManager
    :type _parent: Context
    :type _name: str
    """
    __slots__ = ('_manager', '_parent', '_data', '_name', '_logger')

    def __init__(self, manager, name=None, parent=None):
        """
        Constructor
        :type manager: ContextManager
        :type name: str
        :type parent: Context
        :param manager:
        :param name:
        :param parent:
        """
        self._manager = manager
        self._parent = parent
        self._data = {}
        self._name = self._as_hash() if name is None else name
        self._logger = logging.getLogger(self._name)
        self._logger.parent = logger

    def __getattr__(self, item):
        _data = self._data
        if item in _data:
            return _data[item]
        if self._parent:
            return getattr(self._parent, item)
        self._logger.error(
            'Traceback (most recent call last):\n%sAttributeError in %s: %s\n',
            ''.join(traceback.format_stack()[:-1]),
            repr(self),
            item,
        )
        raise AttributeError(item)

    def __dir__(self):
        rv = set()
        for o in self:
            rv.update(o._data.keys())
        rv.update(self.__slots__)
        return sorted(rv)

    def k2o(self):
        d = dict()
        if self._parent:
            d.update(self._parent.k2o())
        d.update({
            key: self
            for key in self._data.keys()
        })
        return d

    def __setattr__(self, key, value):
        if key in self.__slots__:
            object.__setattr__(self, key, value)
        else:
            self._data[key] = value

    def _as_hash(self):
        return codecs.encode(struct.pack('>q', hash(self)), 'hex').decode('latin1')

    def __iter__(self):
        o = self
        while o:
            yield o
            o = o._parent

    def __repr__(self):
        result = (o._name for o in self)
        return '<Context manager="%s" stack=["%s"]>' % (self._manager.name, '", "'.join(result))


class ContextManager(object):
    """
    Context manager
    """
    def __init__(self, name='<default>'):
        self.name = name
        self.__context_stacks = defaultdict(self._make_default_context_deque)

    _get_thread_id = staticmethod(_default_get_thread_id)

    @property
    def __context_stack(self):
        return self.__context_stacks[self._get_thread_id()]

    def _make_default_context_deque(self):
        return deque((Context(self),))

    @property
    def top(self):
        """
        :rtype: object|Context
        """
        return self.__context_stack[-1]

    @contextmanager
    def __call__(self, name=None):
        context = Context(self, name, self.top)
        self.__context_stack.append(context)
        yield context
        self.__context_stack.pop()

    @contextmanager
    def switch(self, context):
        self.__context_stack.append(context)
        yield context
        self.__context_stack.pop()

    def __repr__(self):
        stack = list(map(repr, self.__context_stack))
        if len(stack) > 1:
            stack.reverse()
            stack = [stack[0], '---- Inactive Contexts: ----'] + stack[1:]
            return '<ContextManager "%s">\n    %s' % (self.name, '\n    '.join(stack))
        else:
            return '<ContextManager "%s">\n    %s' % (self.name, stack[0])


def main():
    manager = ContextManager("My manager")
    context = manager.top
    context.a = 2
    print(context.a)
    print(repr(manager))
    with manager("Level 1"):
        manager.top.a = 3
        manager.top.b = 4
        print(manager.top.a)
        print(manager.top.b)
        print(repr(manager))

        with manager("Level 2") as pluscontext:
            manager.top.c = 100
            print(repr(manager))
            print(dir(manager.top))
            print(manager.top.k2o())

    with manager.switch(pluscontext):
        print(manager.top.c)
        print(manager.top.b)
        print(manager.top.a)
        print(repr(manager))
    print(manager.top.a)
    print(repr(manager))
    import time
    time.sleep(1)
    print(manager.top.b)


if __name__ == '__main__':
    main()
