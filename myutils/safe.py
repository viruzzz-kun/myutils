import datetime
from decimal import (
    Decimal,
)

import six
from typing import (
    Optional,
)


try:
    from dateutil.parser import (
        parse as _parse_datetime,
    )
except ImportError:
    def _parse_datetime(timestr):
        return datetime.datetime.strptime(timestr, '%Y-%m-%d')


def safe_unicode(val, default=None):
    # type: (six.string_types, Optional[six.text_type]) -> Optional[six.text_type]
    if isinstance(val, six.binary_type):
        return val.decode('utf-8', errors='ignore')
    elif isinstance(val, six.text_type):
        return val
    try:
        return six.text_type(val)
    except (ValueError, TypeError):
        return default


def safe_text(val):
    if val is None:
        return None
    return six.text_type(val)


def safe_int(val):
    if val is None:
        return None
    return int(val)


def safe_dict(val):
    if val is None:
        return None
    elif isinstance(val, dict):
        return {
            k: safe_dict(v)
            for k, v in six.iteritems(val)
        }
    elif hasattr(val, '__json__'):
        return safe_dict(val.__json__())
    return val


def safe_bool(val):
    if val is None:
        return None
    if isinstance(val, six.binary_type):
        val = val.decode(errors='ignore')
    if isinstance(val, six.string_types):
        return val.lower() not in (u'0', u'false', u'\x00', u'')
    return bool(val)


def safe_double(val):
    if val is None:
        return None
    if isinstance(val, six.string_types):
        val = val.replace(',', '.')
    try:
        val = float(val)
    except ValueError:
        val = None
    return val


def safe_decimal(val):
    if val is None:
        return None
    val = Decimal(val)
    return val


def safe_date(val):
    if val is None:
        return None
    elif isinstance(val, datetime.datetime):
        return val.date()
    elif isinstance(val, datetime.date):
        return val
    elif isinstance(val, six.string_types):
        return _parse_datetime(val).date()


def safe_datetime(val):
    if val is None:
        return None
    elif isinstance(val, datetime.datetime):
        return val
    elif isinstance(val, datetime.date):
        return datetime.datetime.combine(val, datetime.time())
    elif isinstance(val, six.string_types):
        return _parse_datetime(val)
