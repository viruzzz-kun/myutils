#!/usr/bin/env python
# -*- coding: utf-8 -*-
from jinja2 import (
    environmentfilter,
)
from myutils.traverse.utils import (
    safe_traverse_attrs,
)


__author__ = 'viruzzz-kun'
__created__ = '16.04.17'


class Undefined(object):
    pass


@environmentfilter
def do_attrgetter(environment, obj, name):
    def _g(name):
        for item in obj:
            result = safe_traverse_attrs(item, name, default=Undefined, delimiter='.')
            if result is not Undefined:
                yield result
    try:
        name = str(name)
    except UnicodeError:
        pass
    else:
        return _g(name)
    return environment.undefined(obj=obj, name=name)
