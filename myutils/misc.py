import typing as t


def bail_out(exc):
    # type: (Exception) -> t.NoReturn
    raise exc


class Undefined(object):
    pass
