#!/usr/bin/env python
# -*- coding: utf-8 -*-
import itertools


__author__ = 'viruzzz-kun'
__created__ = '08.04.17'


def _split_parts(parts, delimiter='/'):
    return list(
        itertools.chain.from_iterable(
            part.split(delimiter)
            for part in parts
        )
    )


def _split_last(collection):
    if len(collection) > 0:
        return collection[:-1], collection[-1]
    return [], []


def _head_split(collection):
    if len(collection) > 0:
        return collection[0], collection[1:]
    return [], []


def safe_traverse(obj, *parts, default=None, delimiter='/'):
    parts = _split_parts(parts, delimiter)
    for part in parts:
        try:
            obj = obj[part]
        except KeyError:
            return default
    return obj


def safe_traverse_put(obj, value, *parts, delimiter='/', factory=dict):
    parts = _split_parts(parts, delimiter)
    head, tail = _split_last(parts)
    for part in head:
        try:
            obj = obj[part]
        except KeyError:
            obj = obj[part] = factory()
    obj[tail] = value


def safe_traverse_attrs(obj, *parts, default=None, delimiter='/'):
    parts = _split_parts(parts, delimiter)
    for part in parts:
        try:
            obj = getattr(obj, part)
        except AttributeError:
            return default
    return obj


def safe_traverse_attrs_put(obj, value, *parts, delimiter='/', factory=None):
    parts = _split_parts(parts, delimiter)
    head, tail = _split_last(parts)
    for part in head:
        try:
            obj = getattr(obj, part)
        except AttributeError:
            if not callable(factory):
                raise RuntimeError
            new_obj = factory()
            setattr(obj, part, new_obj)
            obj = new_obj
    setattr(obj, tail, value)


def bail_out(exc):
    raise exc


def first(sequence):
    i = iter(sequence)
    try:
        return next(i)
    except StopIteration:
        return None
