#!/usr/bin/env python
# -*- coding: utf-8 -*-
import jinja2
import yaml


__author__ = 'viruzzz-kun'
__created__ = '08.04.17'


def yaml_import(loader, node):
    filename = loader.construct_scalar(node)
    with open(filename) as f:
        return yaml.load(f)


def file_include(loader, node):
    filename = loader.construct_scalar(node)
    with open(filename) as f:
        return f.read()


def jinja2_include(loader, node):
    context = loader.construct_mapping(node)
    source = context.pop('source', None)
    if source is None:
        filename = context.pop('filename')
        with open(filename) as f:
            source = f.read()
    template = jinja2.Template(source)
    return template.render(**context)


yaml.add_constructor('!import', yaml_import)
yaml.add_constructor('!file', file_include)
yaml.add_constructor('!jinja2', jinja2_include)
