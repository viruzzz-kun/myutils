#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import jinja2
import yaml


__author__ = 'viruzzz-kun'
__created__ = '04.03.17'


def env_getter(loader, node):
    value, default = map(str.strip, (loader.construct_scalar(node).split('|', 1) + [''])[:2])
    result = os.environ.get(value, default)
    return result


def jinja_env_getter(loader, node):
    value = loader.construct_scalar(node)
    template = jinja2.Template(value)
    result = template.render(**os.environ)
    return result


def simple_env_getter(loader, node):
    value = str(loader.construct_scalar(node))
    result = value.format(**os.environ)
    return result

yaml.add_constructor('!env', env_getter)
yaml.add_constructor('!jenv', jinja_env_getter)
yaml.add_constructor('!senv', simple_env_getter)
