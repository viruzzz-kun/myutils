#!/usr/bin/env python
# -*- coding: utf-8 -*-
import six
from flask import (
    request,
)
from werkzeug.utils import (
    cached_property,
)

from ..misc import (
    bail_out,
)
from .api_ext import (
    ApiException,
    ApiResponse,
)


__author__ = 'viruzzz-kun'
__created__ = '14.06.17'


class Resource(object):
    decorators = []
    klass = None

    method_map = {
        'list': ('GET', ''),
        'post': ('POST', ''),
        'get': ('GET', '<int:_id>'),
        'put': ('PUT', '<int:_id>'),
        'delete': ('DELETE', '<int:_id>'),
    }
    
    @property
    def session(self):
        raise NotImplemented

    def populate(self, o, j):
        raise NotImplemented

    def represent(self, o):
        raise NotImplemented

    def select_one(self, _id):
        return self.query.filter(self.klass.id == _id).first()

    def create_one(self):
        return self.klass()

    def list(self):
        result = list(map(self.represent, self.page))
        return ApiResponse(
            result,
            headers={'X-Total-Count': self.total}
        )

    def get(self, _id):
        result = self.select_one(_id) or bail_out(ApiException(404))
        return ApiResponse(self.represent(result))

    def delete(self, _id):
        o = self.select_one(_id) or bail_out(ApiException(404))
        self.session.delete(o)
        self.session.commit()
        return ApiResponse()

    def post(self):
        j = request.get_json()
        o = self.create_one()
        self.populate(o, j)
        self.session.add(o)
        self.session.commit()
        return ApiResponse(self.represent(o), 201)

    def put(self, _id):
        j = request.get_json()
        o = self.select_one(_id) or bail_out(ApiException(404))
        self.populate(o, j)
        self.session.commit()
        return ApiResponse(self.represent(o))

    @cached_property
    def query(self):
        return self.klass.query

    @cached_property
    def filtered_query(self):
        query = self.query
        args = request.args
        try:
            ids = []
            for v in args['ids'].split(','):
                if '-' in v:
                    lower, upper = map(int, v.split('-'))
                    ids.extend(six.moves.range(lower, upper + 1))
                else:
                    ids.append(int(v))
            query = query.filter(self.klass.id.in_(ids))
        except (KeyError, ValueError):
            pass
        return query

    @cached_property
    def page(self):
        query = self.filtered_query
        args = request.args
        try:
            per_page = int(args['_perPage'])
            page = int(args['_page'])
            query.limit(per_page).offset((page-1) * per_page)
        except (KeyError, ValueError):
            pass
        return query

    @cached_property
    def total(self):
        return self.filtered_query.count()

    @cached_property
    def grand_total(self):
        return self.query.count()


class ResourceRegistry(object):
    def __init__(self, app, prefix='/'):
        """
        :type app: flask.Blueprint | flask.Flask
        :param app:
        :param prefix:
        """
        self.app = app
        self.prefix = prefix.strip('/')

    def route(self, pre_path, *class_args, **class_kwargs):
        pre_path = pre_path.strip('/')

        def get_view_func(resource, method):
            def view_func(*args, **kwargs):
                obj = resource(*class_args, **class_kwargs)
                return method(obj, *args, **kwargs)
            for dec in resource.decorators:
                view_func = dec(view_func)
            return view_func

        def decorator(resource):
            for name, (method, url) in six.iteritems(resource.method_map):
                class_method = getattr(resource, name, None)
                if class_method is None:
                    continue
                view_func = get_view_func(resource, class_method)
                view_name = 'api_%s_%s' % (resource.__name__, name)
                view_url = '/%s/%s/%s' % (self.prefix, pre_path, url)
                self.app.add_url_rule(view_url, view_name, view_func, methods=[method])
            return resource

        return decorator



