# coding=utf-8
import json

import six
import typing as t

from .api_ext import (
    ApiExtension,
)


class JsonExtension(ApiExtension):
    """
    This is the Flask Extension to make api_methods that return JSON responses
    """
    content_type = 'application/json; charset=utf8'
    config_prefix = 'JSON_EXT_'

    JsonEncoder = json.JSONEncoder  # You can override it in subclasses

    def serialize(self, response):
        # type: (t.Dict[str, t.Any]) -> six.string_types
        return json.dumps(
            obj=response,
            cls=self.JsonEncoder,
            sort_keys=True,
            ensure_ascii=False,
        )
