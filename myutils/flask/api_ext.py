#!/usr/bin/env python
# -*- coding: utf-8 -*-
import copy
import functools
import logging
import sys
import traceback

import attr
import blinker
import flask
import six
import typing as t

from ..misc import (
    Undefined,
)
from ..safe import (
    safe_unicode,
)


__author__ = 'viruzzz-kun'
__created__ = '23.02.17'

logger = logging.getLogger(__name__)


def extract_traceback():
    t, v, tb = sys.exc_info()
    extracted_tb = traceback.extract_tb(tb)
    return [
        (
            safe_unicode(s.filename),
            safe_unicode(s.name),
            s.lineno,
            safe_unicode(s.line),
        )
        for s in extracted_tb
    ]


# Typing

UAny = t.Union[Undefined, t.Any]
UInt = t.Union[Undefined, int]
UStr = t.Union[Undefined, str]
HeaderType = t.Dict[str, str]
ExtraType = t.Dict[str, t.Any]
UHT = t.Union[Undefined, HeaderType]
UET = t.Union[Undefined, ExtraType]


@attr.s(slots=True)
class ResponseData(object):
    """
    This is the auxiliary class to pass data and meta between ApiResponse and ApiExtension
    """
    data = attr.ib(type=UAny)
    meta = attr.ib(type=t.Dict[str, t.Any])


class ApiResponse(object):
    """
    This is the class that wraps responses from API methods so they can pass additional metadata,
    HTTP headers and HTTP status code.
    """
    code = 200
    name = 'OK'
    data = Undefined
    headers = Undefined  # type: dict
    extra = Undefined  # type: dict

    def __init__(
            self,
            data=Undefined,
            code=Undefined,
            name=Undefined,
            headers=Undefined,
            extra=Undefined):
        # type: (UAny, UInt, UStr, UHT, UET) -> None
        """
        Constructor

        :param data: Response data to serialize
        :param code: HTTP status code and also code for meta dict
        :param name: status name for meta dict
        :param headers: dict of additional HTTP headers
        :param extra: dict of additional meta data
        """
        self.data = data
        if data is Undefined:
            if code is Undefined:
                code = 204
        if code is not Undefined:
            self.code = code
        if name is not Undefined:
            self.name = name
        if headers is not Undefined:
            self.headers = headers
        if extra is not Undefined:
            self.extra = extra

    def collapse(self):
        # type: () -> ResponseData
        """
        This method is used to format ApiResponse information to ResponseData, so that ApiExtension
        could further format it as actual response. It can be overridden in subclasses to perform
        customized formatting.
        """
        _meta = {
            'code': self.code,
            'name': self.name
        }
        if self.extra is not Undefined:
            _meta.update(self.extra)
        return ResponseData(self.data, _meta)


class ApiError(ApiResponse, Exception):
    """
    This is the class used by API methods to represent known errors connected with particular API
    method. It doesn't return `data` to client but gives additional error description in meta's
    `desc` key.
    """
    code = 500
    name = 'Internal Server Error'
    desc = 'Internal Server Error'

    def __init__(
            self,
            code=Undefined,
            name=Undefined,
            desc=Undefined,
            headers=Undefined,
            extra=Undefined):
        # type: (UInt, UStr, UStr, UHT, UET) -> None
        super(ApiError, self).__init__(code=code, name=name, headers=headers, extra=extra)
        if desc is not Undefined:
            self.desc = desc

    def collapse(self):
        # type: () -> ResponseData
        """
        This method deliberately removes `data` from ResponseData and sets `desc` key to `meta` dict
        """
        result = super(ApiError, self).collapse()
        result.meta['desc'] = self.desc
        result.data = Undefined
        return result

    @classmethod
    def wrap_exception(cls, e):
        # type: (Exception) -> ApiError
        """
        THis method is used to convert exception to an ApiError, setting `desc` to exception's
        string representation
        :param e: Exception to wrap
        :return: ApiError object
        """
        return cls(code=500, desc=str(e))


class ApiException(ApiError):
    """
    This is the class to represent some unexpected Exception.

    :cvar debug: When set to True, include traceback in the `meta`
    """
    debug = False

    def collapse(self):
        # type: () -> ResponseData
        result = super(ApiException, self).collapse()
        if self.debug:
            result.meta['traceback'] = extract_traceback()  # DIRTY
        return result


class ApiExtension(object):
    """
    This is the class of Flask extension to wrap API methods.
    It must be subclassed to implement actual serialization routines and set `content_type` cvar.

    Usage:
        from flask import Flask
        from myutils.flask.json_ext import JsonExtension

        app = Flask()

        jx = JsonExtension(app)

        @app.route('/')
        @jx.api_method()
        def root():
            return {'key': 'value')

        @app.route('/error')
        @jx.api_method()
        def error():
            raise jx.ApiError(400, 'General error', 'Something went wrong')

    :cvar content_type: Value for Content-Type HTTP header
    :cvar config_prefix: Default prefix to get namespace from Flask configuration
    :cvar ApiResponse: ApiResponse (sub)class
    :cvar ApiError: ApiError (sub)class
    :cvar ApiException: ApiException (sub)class
    """
    content_type = None
    config_prefix = 'API_EXT_'

    ApiResponse = ApiResponse
    ApiError = ApiError
    ApiException = ApiException

    before_response = blinker.Signal()
    after_response = blinker.Signal()

    def __init__(self, app=None):
        self.app = app

        if app is not None:
            self.init_app(app)

    def init_app(self, app, prefix=None):
        # type: (flask.Flask, str) -> None
        self.app = app

        if prefix is None:
            prefix = self.config_prefix

        ns = app.config.get_namespace(prefix)
        self.ApiException.debug = ns.setdefault('DEBUG', app.debug)

    def api_method(self, f=None):
        def decorator(func):
            func.is_api = True

            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                try:
                    result = func(*args, **kwargs)

                except ApiException as e:
                    logger.exception('ApiException occurred while making API response: {}'.format(e))
                    return self.make_response(e)

                except ApiError as e:
                    logger.debug('ApiError occurred while making API response: {}'.format(e))
                    return self.make_response(e)

                except Exception as e:
                    logger.exception('Exception occurred while making API response: {}'.format(e))
                    return self.make_response(self.ApiException.wrap_exception(e))

                else:
                    if not isinstance(result, ApiResponse):
                        result = self.ApiResponse(result)
                    return self.make_response(result)

            return wrapper

        if f is None:
            return decorator

        else:
            return decorator(f)

    def serialize(self, response):
        # type: (t.Dict[str, t.Any]) -> six.string_types
        """
        Implement this method in a subclass to actually perform serialization of responses from
        Api methods

        :param response: dict to serialize
        :return: bytes for Response body
        """
        raise NotImplementedError

    def make_response(self, api_response):
        # type: (ApiResponse) -> flask.Response
        """
        This method converts `ApiResponse` (or subclass) to `flask.Response` - calls
        `ApiResponse.collapse()` to get `meta` and dat`a from `ResponseData`, then calls
        `_response_data_as_dict()` to get final dict form, and then calls `serialize` to serialize
        it and makes `flask.Response` finally

        :param api_response: ApiResponse (or subclassed) object
        :return: flask.Response object
        """
        flask_request = flask.request
        self.before_response.send(
            self,
            data=api_response.data,
            code=api_response.code,
            name=api_response.name,
            headers=api_response.headers,
            extra=api_response.extra,
            request=flask_request,
        )
        as_dict = self._api_response_as_dict(api_response)
        flask_response = flask.current_app.response_class(
            response=self.serialize(as_dict),
            status=api_response.code,
            content_type=self.content_type
        )  # type: flask.Response
        if api_response.headers is not Undefined:
            flask_response.headers.extend(api_response.headers)
        self.after_response.send(
            self,
            request=flask_request,
            response=flask_response,
        )
        return flask_response

    def _api_response_as_dict(self, api_response):
        # type: (ApiResponse) -> t.Dict[str, t.Any]
        """
        Override this method if you want to completely change the way ApiResponse (and subclasses)
        are converted to dicts

        :param api_response: ApiResponse to unstructure
        :return: unstructured data
        """
        return self._response_data_as_dict(api_response.collapse())

    def _response_data_as_dict(self, response_data):
        # type: (ResponseData) -> t.Dict[str, t.Any]
        """
        This method may be overridden to rename `'_meta'` and `'_result'` fields or make completely
        different final dict form of response data

        :param response_data: ResponseData object
        :return: dict in Final Form :)
        """
        result = {
            '_meta': response_data.meta
        }
        if response_data.data is not Undefined:
            result['_result'] = response_data.data
        return result


