#!/usr/bin/env python
# -*- coding: utf-8 -*-
from functools import (
    partial,
)

from flask import (
    request,
)
from flask.globals import (
    _lookup_req_object,
    _request_ctx_stack,
)
from werkzeug.local import (
    LocalProxy,
)
from werkzeug.utils import (
    cached_property,
)


__author__ = 'viruzzz-kun'
__created__ = '16.03.17'


class LanguageInfo(object):
    @cached_property
    def lang(self):
        raise NotImplemented()

    def switch_lang(self, language):
        raise NotImplemented()

    @classmethod
    def from_request(cls, request):
        raise NotImplemented()


language_info = LocalProxy(partial(_lookup_req_object, 'language_info'))


class LangExtension(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)
        self._lang_info_class = None  # type: LanguageInfo

    def init_app(self, app):
        """
        :type app: flask.Flask
        :param app:
        :return:
        """
        self.app = app
        app.before_request(self._before_request)
        app.context_processor(self.context_processor)

    def _before_request(self):
        lang_info = self._lang_info_class.from_request(request)  # type: LanguageInfo
        self._set_req_object('language_info', lang_info)
        self._set_req_object('lang', lang_info.lang)
        self._set_req_object('switch', lang_info.switch_lang)

    @staticmethod
    def context_processor():
        return {
            'switch_lang': language_info.switch_lang,
            'lang': language_info.lang,
        }

    @staticmethod
    def _set_req_object(name, value):
        top = _request_ctx_stack.top
        if top is None:
            raise RuntimeError('working outside of request context')
        return setattr(top, name, value)

    @staticmethod
    def _get_req_object(name, default=None):
        top = _request_ctx_stack.top
        if top is None:
            raise RuntimeError('working outside of request context')
        return getattr(top, name, default=default)

    def lang_info_class(self, cls):
        self._lang_info_class = cls
        return cls

