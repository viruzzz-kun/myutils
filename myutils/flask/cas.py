# -*- coding: utf-8 -*-
import logging
from functools import (
    partial,
)

import flask
import requests
from flask import (
    redirect,
    request,
)
from flask.globals import (
    _lookup_req_object,
    _request_ctx_stack,
)
from flask.signals import (
    Namespace,
)
from werkzeug.local import (
    LocalProxy,
)


__author__ = 'viruzzz-kun'


logger = logging.getLogger('CasExtension')
logger.setLevel(logging.INFO)
if not logger.handlers:
    logger.addHandler(logging.StreamHandler())


current_user = LocalProxy(partial(_lookup_req_object, 'cas_current_user'))


class CasError(Exception):
    pass


class CasNotAvailable(CasError):
    message = u'Castiel not available'


class CasUser(object):
    user_id = None
    success = False
    login = None
    deadline = None
    ttl = None
    token = None


class CasExtension(object):
    """
    Extension for working with CAS Castiel
    """
    __signals = Namespace()

    user_id_set = __signals.signal('user-id-set')
    user_id_unset = __signals.signal('user-id-unset')

    cookie_name = 'CastielAuthToken'
    cas_external_address = 'http://127.0.0.1:5001/'
    cas_internal_address = 'http://127.0.0.1:5001/'
    permissive = True
    app = None

    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app, prefix='CAS_'):
        """
        Install Extension at app
        :type app: flask.blueprints.Blueprint | flask.app.Flask
        :param app: Application
        :param prefix: Flask configuration namespace prefix
        :return:
        """
        self.app = app
        ns = app.config.get_namespace(prefix)
        self.cookie_name = ns.get('auth_cookie', self.cookie_name)
        self.cas_external_address = ns.get('external_url', self.cas_external_address).rstrip('/')
        self.cas_internal_address = ns.get('internal_url', self.cas_internal_address).rstrip('/')
        self.permissive = ns.get('permissive', self.permissive)
        logout_endpoint = ns.get('logout_endpoint', '/logout')

        if not hasattr(app, 'extensions'):
            app.extensions = {}
        app.extensions['cas'] = self
        app.before_request(self._before_request)
        app.errorhandler(CasNotAvailable)(self._cas_not_available)
        app.context_processor(self._cas_context)
        app.route(logout_endpoint, methods=['GET'])(self._logout_endpoint)

    @staticmethod
    def public_endpoint(func):
        """
        Decorator for view endpionts which should be marked as public, thus CAS won't enforce its policies
        :param func: decorated endpoint view function
        """
        func.is_public = True
        return func

    @staticmethod
    def login_required(func):
        """
        Decorator for view endpoint which should me protected by CAS policies
        :param func: decorated endpoint view function
        """
        func.login_required = True
        return func

    def _before_request(self):
        """
        Handler to be executed before each request
        """
        new_user = CasUser()
        self._set_req_object('cas_current_user', new_user)

        endpoint = request.endpoint

        if not (endpoint and 'static' not in endpoint):
            return
        token = request.cookies.get(self.cookie_name)

        in_protected_endpoint = self._is_protected_endpoint(endpoint)
        if not token:
            if in_protected_endpoint:
                return redirect(self.__login_redirect_url())
        else:
            # Token is set
            try:
                result = requests.post(
                    self.cas_internal_address + '/cas/api/check',
                    json={'token': token, 'prolong': True},
                    headers={'Referer': request.url.encode('utf-8')}
                )
            except requests.ConnectionError:
                raise CasNotAvailable

            answer = result.json()
            new_user.__dict__.update(answer)
            if answer.get('success'):
                self.user_id_set.send(self.app, id=new_user.user_id)
            else:
                self.user_id_set.send(self.app, id=None)
                self.user_id_unset.send(self.app)
                if in_protected_endpoint:
                    return redirect(self.__login_redirect_url())

    def __login_redirect_url(self, url=None):
        return u'%s/cas/login?back=%s' % (self.cas_external_address, url or request.url)

    def __logout_redirect_url(self, url=None):
        return flask.url_for('_logout_endpoint')

    def _cas_not_available(self, e):
        return u'Невозможно связаться с подсистемой централизованной аутентификации'

    def _cas_context(self):
        return {
            'cas': {
                'login_url': self.__login_redirect_url(),
                'logout_url': self.__logout_redirect_url(),
                'logged_in': current_user.success,
            },
            'cas_user': current_user,
        }

    def _is_protected_endpoint(self, endpoint):
        view_function = self.app.view_functions[endpoint]
        if self.permissive:
            return getattr(view_function, 'login_required', False)
        else:
            return not getattr(view_function, 'is_public', False)

    @staticmethod
    def _set_req_object(name, value):
        top = _request_ctx_stack.top
        if top is None:
            raise RuntimeError('working outside of request context')
        return setattr(top, name, value)

    def _logout_endpoint(self):
        try:
            result = requests.post(
                self.cas_internal_address + '/cas/api/release',
                json={'token': current_user.token}
            )
        except requests.ConnectionError:
            flask.flash('Cannot connect to CAS', category='danger')
        except:
            flask.flash('Strange error', category='danger')
        else:
            if result.status_code != 200:
                flask.flash('Error Logging out', category='danger')
        return redirect(request.referrer)
