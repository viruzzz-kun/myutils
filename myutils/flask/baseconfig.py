#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml


__author__ = 'viruzzz-kun'
__created__ = '12.04.17'


class BaseConfig(object):
    def __init__(self):
        self.config = {}

    def load(self, filename):
        with open(filename) as fin:
            config = self.config = yaml.load(fin)
        self.configure(config)

    def configure(self, config):
        pass
