#!/usr/bin/env python
# -*- coding: utf-8 -*-
import yaml
# noinspection PyUnresolvedReferences
from myutils.yaml import (
    yaml_env,
    yaml_files,
)


class Config(object):
    def __init__(self, app=None):
        """
        :type app: flask.Flask
        :param app:
        :return:
        """
        self._hooks = []
        self._configured = False
        self.config = None
        self.app = app

    def init_app(self, app):
        """
        :type app: flask.Flask
        :param app:
        :return:
        """
        self.app = app
        self.attempt_config()

    def load(self, filename):
        with open(filename) as fin:
            self.config = yaml.load(fin)
        self.attempt_config()

    def attempt_config(self):
        if not self._configured and \
                self.config is not None and \
                self.app is not None:

            self.app.config.from_mapping(self.config)
            for hook in self._hooks:
                hook()
            self._configured = True

    def on_configuration(self, hook):
        self._hooks.append(hook)
        return hook
