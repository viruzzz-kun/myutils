import functools

import six


class TransparentWrapper:
    def __init__(self, source):
        self.__source__ = source

    def __getattr__(self, item):
        return getattr(self.__source__, item)

    @classmethod
    def factory(cls, **overrides):
        @functools.wraps(cls)
        def _(*args, **kwargs):
            self = cls(*args, **kwargs)
            self.__dict__.update(overrides)
            return self
        return _


class IdCodeNameJsonProxy(TransparentWrapper):
    def __json__(self):
        return {
            'id': self.id,
            'code': self.code,
            'name': self.name,
        }


class TypedListJsonProxy(object):
    def __init__(self, cls, source):
        self.cls = cls
        self.source = source

    def __json__(self):
        return list(map(self.cls, self.source))


class TypedDictJsonProxy(object):
    def __init__(self, cls, source):
        self.cls = cls
        self.source = source

    def __json__(self):
        return {
            k: self.cls(v)
            for k, v in six.iteritems(self.source)
        }
