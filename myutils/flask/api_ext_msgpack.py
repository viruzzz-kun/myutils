# coding=utf-8
import msgpack
import typing as t

from .api_ext import (
    ApiExtension,
)


class MsgPackApiExtension(ApiExtension):
    """
    This is the Flask Extension that can be used to make api_methods that
    return MsgPack'ed responses
    """
    content_type = 'application/msgpack'
    config_prefix = 'MSGPACK_API_EXT_'

    def serialize(self, response):
        # type: (t.Dict[str, t.Any]) -> bytes
        return msgpack.dumps(response)
